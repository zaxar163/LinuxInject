package by.radioegor146.magictheinjecting;

import java.io.DataInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;
import java.security.ProtectionDomain;
import java.util.ArrayList;

@SuppressWarnings("rawtypes")
public class MagicTheInjecting extends Thread {

	public static int injectCP() {
		try {
			final MagicTheInjecting t = new MagicTheInjecting();
			t.start();
		} catch (final Exception t) {
			// empty catch block
		}
		return 0;
	}

	private static byte[] readByteArray(final DataInputStream in) throws IOException {
		final byte[] ret = new byte[in.readInt()];
		in.readFully(ret);
		return ret;
	}

	private static Class tryGetClass(final ClassLoader cl, final String... names) throws ClassNotFoundException {
		ClassNotFoundException lastException = null;
		for (final String name : names)
			try {
				return cl.loadClass(name);
			} catch (final ClassNotFoundException e) {
				lastException = e;
			}
		throw lastException;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		try {
			try {
				ClassLoader cl = null;
				for (final Thread thread : Thread.getAllStackTraces().keySet()) {
					ClassLoader threadLoader;
					if (thread == null || thread.getContextClassLoader() == null
							|| (threadLoader = thread.getContextClassLoader()).getClass() == null
							|| threadLoader.getClass().getName() == null)
						continue;
					final String loaderName = threadLoader.getClass().getName();
					if (!loaderName.contains("LaunchClassLoader") && !loaderName.contains("RelaunchClassLoader"))
						continue;
					cl = threadLoader;
					break;
				}
				if (cl == null)
					throw new Exception("ClassLoader is null");
				setContextClassLoader(cl);
				final Class forgeEventHandlerAnnotation = tryGetClass(cl, "cpw.mods.fml.common.Mod$EventHandler",
						"net.minecraftforge.fml.common.Mod$EventHandler");
				final Class modAnnotation = tryGetClass(cl, "cpw.mods.fml.common.Mod",
						"net.minecraftforge.fml.common.Mod");
				final Class fmlInitializationEventClass = tryGetClass(cl,
						"cpw.mods.fml.common.event.FMLInitializationEvent",
						"net.minecraftforge.fml.common.event.FMLInitializationEvent");
				final Class fmlPreInitializationEventClass = tryGetClass(cl,
						"cpw.mods.fml.common.event.FMLPreInitializationEvent",
						"net.minecraftforge.fml.common.event.FMLPreInitializationEvent");
				final Method loadMethod = ClassLoader.class.getDeclaredMethod("defineClass", String.class, byte[].class,
						Integer.TYPE, Integer.TYPE, ProtectionDomain.class);
				loadMethod.setAccessible(true);
				final ArrayList<Object[]> mods = new ArrayList<>();
				try (Socket s = new Socket("localhost", 25577)) {
					final DataInputStream in = new DataInputStream(s.getInputStream());
					final int cnt = in.readInt();
					for (int i = 0; i < cnt; i++) {
						final byte[] classData = readByteArray(in);
						final String name = in.readUTF();
						if (classData == null)
							throw new Exception("classData is null");
						if (cl.getClass() == null)
							throw new Exception("getClass() is null");
						try {
							final Class tClass = (Class) loadMethod.invoke(cl, name, classData, 0, classData.length,
									cl.getClass().getProtectionDomain());
							if (tClass.getAnnotation(modAnnotation) == null)
								continue;
							final Object[] mod = new Object[3];
							mod[0] = tClass;
							final ArrayList<Method> fmlPreInitMethods = new ArrayList<>();
							final ArrayList<Method> fmlInitMethods = new ArrayList<>();
							for (final Method m : tClass.getDeclaredMethods()) {
								if (m.getAnnotation(forgeEventHandlerAnnotation) != null && m.getParameterCount() == 1
										&& m.getParameterTypes()[0] == fmlInitializationEventClass) {
									m.setAccessible(true);
									fmlInitMethods.add(m);
								}
								if (m.getAnnotation(forgeEventHandlerAnnotation) != null && m.getParameterCount() == 1
										&& m.getParameterTypes()[0] == fmlPreInitializationEventClass) {
									m.setAccessible(true);
									fmlPreInitMethods.add(m);
								}
							}
							mod[1] = fmlPreInitMethods;
							mod[2] = fmlInitMethods;
							mods.add(mod);
						} catch (final Exception e) {
							e.printStackTrace();
							throw new Exception("Exception on defineClass", e);
						}
					}
				}
				for (final Object[] mod : mods) {
					final Class modClass = (Class) mod[0];
					final ArrayList<Method> fmlPreInitMethods = (ArrayList<Method>) mod[1];
					final ArrayList<Method> fmlInitMethods = (ArrayList<Method>) mod[2];
					Object modInstance = null;

					try {
						modInstance = modClass.newInstance();
					} catch (final Exception e) {
						throw new Exception("Exception on instancing", e);
					}

					for (final Method preInitMethod : fmlPreInitMethods)
						try {
							preInitMethod.invoke(modInstance, new Object[] { null });
						} catch (final InvocationTargetException e) {
							throw new Exception("Exception on preiniting (InvocationTargetException)", e.getCause());
						} catch (final Exception e) {
							throw new Exception("Exception on preiniting", e);
						}

					for (final Method initMethod : fmlInitMethods)
						try {
							initMethod.invoke(modInstance, new Object[] { null });
						} catch (final InvocationTargetException e) {
							throw new Exception("Exception on initing (InvocationTargetException)", e.getCause());
						} catch (final Exception e) {
							throw new Exception("Exception on initing", e);
						}
				}
			} catch (final Throwable e) {
				e.printStackTrace();
			}
		} catch (final Throwable e) {
			e.printStackTrace();
		}
	}

}