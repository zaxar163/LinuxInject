package by.radioegor146.headerconverter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ConverterJar {
	public static List<ClassPair> convert(final File in) throws IOException {
		try (final ZipInputStream zipFile = new ZipInputStream(new FileInputStream(in))) {
			final List<ClassPair> classes = new ArrayList<>();
			final HashMap<String, ClassPair> classMap = new HashMap<>();
			ZipEntry entry;
			while ((entry = zipFile.getNextEntry()) != null)
				if (entry.getName().endsWith(".class")) {
					final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
					final byte[] data = new byte[16384];
					int nRead;
					while ((nRead = zipFile.read(data, 0, data.length)) != -1)
						buffer.write(data, 0, nRead);
					final byte[] classData = buffer.toByteArray();
					classes.add(new ClassPair(classData));
					final ClassInfo cinfo = new ClassInfo(classes.get(classes.size() - 1).classData);
					classes.get(classes.size() - 1).classInfo = cinfo;
					classMap.put(cinfo.name, classes.get(classes.size() - 1));
				}
			classes.forEach(_item -> classes.forEach(cpair -> {
				ClassInfo cinfo2;
				String[] interfaces;
				int length;
				int k = 0;
				String iface;
				cinfo2 = cpair.classInfo;
				if (classMap.containsKey(cinfo2.superClass))
					classMap.get(cinfo2.superClass).priority = Math.max(classMap.get(cinfo2.superClass).priority,
							cpair.priority + 1);
				interfaces = cinfo2.interfaces;
				for (length = interfaces.length; k < length; ++k) {
					iface = interfaces[k];
					if (classMap.containsKey(iface))
						classMap.get(iface).priority = Math.max(classMap.get(iface).priority, cpair.priority + 1);
				}
			}));
			Collections.sort(classes);
			return classes;
		}
	}
}