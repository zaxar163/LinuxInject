package ru.zaxar163.linuxinj;

import java.util.Arrays;

import ru.zaxar163.utils.LogHelper;

public class Main {
	private static void help() {
		System.out.println("Modes: jarsocketp, jarsocketn, stub");
		System.out.println("Usage: <mode>");
		System.exit(1);
	}

	public static void main(final String[] args) {
		try {
			if (args.length < 1)
				help();
			final String[] realArgs = Arrays.copyOfRange(args, 1, args.length);
			switch (args[0]) {
			case "jarsocketp":
				JarSocket.startPid(realArgs);
				break;
			case "jarsocketn":
				JarSocket.startName(realArgs);
				break;
			case "stub":
				StubGenerator.main(realArgs);
				break;
			default:
				help();
				break;
			}
		} catch (final Throwable e) {
			LogHelper.error(e);
		}
	}
}