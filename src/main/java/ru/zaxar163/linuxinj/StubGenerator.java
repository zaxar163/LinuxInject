package ru.zaxar163.linuxinj;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.objectweb.asm.Type;

import by.radioegor146.headerconverter.ConverterClass;
import by.radioegor146.magictheinjecting.MagicTheInjecting;
import ru.zaxar163.utils.IOHelper;

public class StubGenerator {
	public static void main(final String[] args) throws IOException {
		if (args.length < 1) {
			System.out.println("Usage: stub [out dir]");
			System.exit(1);
		}
		final Path tar = Paths.get(args[0]);
		final Path out = tar.resolve("lib");
		unpackZipNoCheck("stub.zip", out);
		final Path inj = tar.resolve("inj");
		unpackZipNoCheck("inj.zip", inj);
		Path target = inj.resolve("Makefile");
		Files.deleteIfExists(target);
		IOHelper.transfer(IOHelper.getResourceBytes("injector.make"), target, false);
		// build inj.
		ProcessBuilder pb = new ProcessBuilder();
		pb.inheritIO();
		pb.directory(inj.toFile());
		pb.command("make");
		try {
			pb.start().waitFor();
		} catch (final InterruptedException e) {
			return;
		}
		// generate stub.
		target = out.resolve("loader.h");
		Files.deleteIfExists(target);
		ConverterClass.convert(IOHelper.getResourceBytes(Type.getInternalName(MagicTheInjecting.class) + ".class"),
				target, "classLoaderClass");
		pb = new ProcessBuilder();
		pb.inheritIO();
		pb.directory(out.toFile());
		pb.command("make");
		try {
			pb.start().waitFor();
		} catch (final InterruptedException e) {
			return;
		}
		IOHelper.copy(inj.resolve("inject"), tar.resolve("injector"));
		IOHelper.copy(out.resolve("injectc.so"), tar.resolve("forInj.so"));
		PermsSet.fix(tar);
	}

	public static boolean unpackZipNoCheck(final String resource, final Path target) throws IOException {
		try {
			if (Files.isDirectory(target))
				IOHelper.deleteDir(target, true);
			Files.deleteIfExists(target);
			try (ZipInputStream input = IOHelper.newZipInput(IOHelper.getResourceURL(resource))) {
				for (ZipEntry entry = input.getNextEntry(); entry != null; entry = input.getNextEntry()) {
					if (entry.isDirectory())
						continue; // Skip dirs
					// Unpack file
					IOHelper.transfer(input, target.resolve(IOHelper.toPath(entry.getName())));
				}
			}
			return true;
		} catch (final NoSuchFileException e) {
			return false;
		}
	}
}
