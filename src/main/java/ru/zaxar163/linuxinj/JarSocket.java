package ru.zaxar163.linuxinj;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import by.radioegor146.headerconverter.ClassPair;
import by.radioegor146.headerconverter.ConverterJar;
import ru.zaxar163.utils.IOHelper;
import ru.zaxar163.utils.LogHelper;

public class JarSocket {
	private static void check(final File wd, final String string) throws IOException {
		final Path w = wd.toPath();
		if (!Files.isDirectory(w) || !Files.isReadable(w.resolve("injector"))
				|| !Files.isReadable(w.resolve("forInj.so"))) {
			if (Files.isDirectory(w))
				IOHelper.deleteDir(w, true);
			else
				Files.deleteIfExists(w);
			StubGenerator.main(new String[] { string });
		}
	}

	public static void startName(final String[] args) throws IOException {
		if (args.length < 3) {
			System.out.println("Usage: jarsocketn [proc name] [work dir (from stub generator)] [jarfile]");
			System.exit(1);
		}
		check(new File(args[1]), args[1]);
		final List<ClassPair> classes = ConverterJar.convert(new File(args[2]));
		final Thread thr = new Thread(() -> {
			try (ServerSocket s = new ServerSocket(25577)) {
				s.setSoTimeout(10000);
				try (Socket c = s.accept()) {
					final DataOutputStream out = new DataOutputStream(c.getOutputStream());
					out.writeInt(classes.size());
					for (final ClassPair p : classes) {
						out.writeInt(p.classData.length);
						out.write(p.classData);
						out.writeUTF(p.classInfo.name);
					}
				}
			} catch (final Throwable e) {
				LogHelper.error(e);
			}
		});
		thr.start();
		final ProcessBuilder pb = new ProcessBuilder();
		pb.command(args[1] + "/injector", "-p", args[0], args[1] + "/forInj.so");
		try {
			pb.start().waitFor();
			thr.join();
		} catch (final InterruptedException e) {
			return;
		}
	}

	public static void startPid(final String[] args) throws IOException {
		if (args.length < 3) {
			System.out.println("Usage: jarsocketp [proc pid] [work dir (from stub generator)] [jarfile]");
			System.exit(1);
		}
		check(new File(args[1]), args[1]);
		final List<ClassPair> classes = ConverterJar.convert(new File(args[2]));
		final Thread thr = new Thread(() -> {
			try (ServerSocket s = new ServerSocket(25577)) {
				s.setSoTimeout(10000);
				try (Socket c = s.accept()) {
					final DataOutputStream out = new DataOutputStream(c.getOutputStream());
					out.writeInt(classes.size());
					for (final ClassPair p : classes) {
						out.writeInt(p.classData.length);
						out.write(p.classData);
						out.writeUTF(p.classInfo.name);
					}
				}
			} catch (final Throwable e) {
				LogHelper.error(e);
			}
		});
		thr.start();
		final ProcessBuilder pb = new ProcessBuilder();
		pb.command(args[1] + "/injector", "-p", args[0], args[1] + "/forInj.so");
		try {
			pb.start().waitFor();
			thr.join();
		} catch (final InterruptedException e) {
			return;
		}
	}
}
