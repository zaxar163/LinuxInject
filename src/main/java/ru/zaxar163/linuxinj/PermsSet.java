package ru.zaxar163.linuxinj;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class PermsSet {
	private static final class SetPermVisitor extends SimpleFileVisitor<Path> {
		private static final Set<PosixFilePermission> DPERMS;

		static {
			final Set<PosixFilePermission> perms = new HashSet<>(Arrays.asList(PosixFilePermission.values()));
			perms.remove(PosixFilePermission.OTHERS_WRITE);
			perms.remove(PosixFilePermission.GROUP_WRITE);
			DPERMS = Collections.unmodifiableSet(perms);
		}

		private SetPermVisitor() {

		}

		@Override
		public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
			Files.setPosixFilePermissions(file, DPERMS);
			return super.visitFile(file, attrs);
		}
	}

	public static void fix(final Path tLib) {
		try {
			Files.walkFileTree(tLib, Collections.singleton(FileVisitOption.FOLLOW_LINKS), Integer.MAX_VALUE,
					new SetPermVisitor());
		} catch (final IOException e) {
			e.printStackTrace(System.err);
		}
	}
}
